---
title: Welcome to the site of a MathemaNiskin!
type: page
feature_text: |
  "The Axiom of Choice is obviously true, the well-ordering principle obviously false, and who can tell about Zorn's lemma?" - Jerry Bona
feature_image: "/assets/pics/skyplane.JPG"  #"https://unsplash.it/1300/400?image=971"
excerpt: "A boilerplate for my mind. Here is where I will blog about whatever: Math, Statistics, Economics, Computer Science, Algorithms, etc."
---

Welcome to my home on the internet. I'll be posting sporadically about whatever interests me. Generally that'll be more STEM oriented, but I imagine once I get more used to this whole blogging thing, I'll branch out. For now the posts are mainly about Math, Algorithms, Optimization, Statistics, Sampling, etc.

Enjoy the site and please feel free to [email me](mailto:aaron@niskin.org), if you come across any issues, typos, mistakes, etc. or if you just feel like talking with me. I'm always up to chatting with new people.

If you're interested in tutoring, visit [Math-Busters](http://www.math-busters.com). If you're in the San Francisco Bay area, and would like in-person tutoring, on the other hand, feel free to email me for a recommendation.
