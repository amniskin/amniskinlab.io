---
title: Nadal vs Federererererer
type: post
excerpt: |
  Nadal serves up some heat, will Federer play it cool?
category: Riddles
tags:
- puzzles
- riddles
- statistics
- fun&games
- pokeaneyeout
feature_image: /assets/pics/2018/07/22_nadal_federer_match.jpg
---

## The scenario:

Nadal and Federer are to play a fun little game of Tennis, but they don't have enough time to go back and forth forever, so this game will be best out of 15. They volley for serve and Nadal wins. Nadal, being the happy sport he is, offers Federer a choice: either they can play the usual rules of back-and-forth serving (regardless of who wins the set), or winner serves again. Knowing that Federer is an extremely logical person, and that each player has a higher chance of winning when that player serves (holds for both Nadal and Federer), what does Federer choose?

What you know:

1. 

What's your strategy?

### Hints (click to unblur)

1. There's a non-zero probability (assuming the choice of prisoner is random) that it's 100,000 years before all prisoners go into that room. So don't try to find a perfect solution. Just any solution will work.
    {: .hint}


### Some Solutions

#### Solution 1

<div class="hint" markdown="1">
</div>
