---
title: "Reducing the False Dropout Rate in regressions"
type: post
excerpt: Let's cover HOMOLOGY! And some of that sweet Betti number goodness.
category: Statistics
tags:
- Statistics
- Mathematics
- Regression
- Inference
feature_image: "/assets/pics/2018/04/12_banner.jpg"
---

# Homology

## What is it?

First, let's talk about the pre-requisites. If you don't already know what a Simplicial Complex is, check out my previous post, [homology pt 1]({{ site.baseurl }}{% post_url 2018-04-12-homology %}) because this post will build off of that.

Great! Now let's talk about Homology. There is a natural notion of equivalence of topological spaces called homotopy, but this definition is very hard to work with in high dimensions. So, the field of Homological Algebra was created (by Henri Poincar&eacute; and, of course, Emmy Noether -- not together, but they were both very influential in the creation).

### Review

Last time we mentioned that we can triangulate the sphere, torus, etc. using the very simple square with associations, but that our chosen triangulations did not result in simplicial complexes. Let's investigate why, specifically, we'll investigate why the triangulation of the tube didn't result in a simplicial complex, and how we can fix it. Then we'll leave the rest for you if you feel up to it.

Last time we used the following triangulation:

![triang]({{ site.baseurl }}/assets/pics/2018/06/25_tube.png)

But notice, the intersection of 2-simplex $A$ with 2-simplex $B$ is the following \\(\\{\langle1,3\rangle,\langle1,2\rangle(\equiv\langle4,3\rangle)\\}\\), which is not a simplex, but a simplicial complex. So we need to do something to get rid of that problem.

The trick I tend to use (not because of its elegance -- because it's really not elegant at all) is this:

![triang]({{ site.baseurl }}/assets/pics/2018/06/25_tube_triangulated.png)

It's super annoying to work with, but it does make it a simplicial complex. Moving on...

## Summary

So we've triangulated some things, defined what simplices and simplicial complexes are, but we haven't yet found their utility. For that, just stay tuned! In the next episode we'll go over Betti Numbers and how to compute them using these simplicial complexes!
