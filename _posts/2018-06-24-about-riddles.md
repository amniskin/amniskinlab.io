---
title: "About the riddles category"
type: post
excerpt: In a world filled with lame riddles, where can you find good ones?!
category: Riddles
tags:
- meta
- puzzles
- riddles
- fun&games
- pokeaneyeout
- nolameriddles
feature_image: /assets/pics/2018/06/24_kids_bike.png
---

Far too many riddles are exceedingly lame. Scenarios like "John is twice as old as Julia, and 5 years older than Tim, how old is Sally?" are really just super lame and uninteresting. They're work, not play. It's not even that they're difficult; it's just that they're annoying. You just write it down and it's very basic linear algebra. Every time.

So for that reason, you will not find any of these atrocities on this site. This site will be exclusively for riddles I find to be fun.

I also won't be posting riddles that start out with a lie as the premise. So there are no tricks nor gimmicks to the riddles I'll be posting here.

And finally, I won't be posting riddles like "what stands on four legs in the morning, two in the afternoon, and three in the evening?" Not for any real reason; I just don't want to.

That's all until one day I do. It's my site, I do what I want!

If you have any riddles you think are cool, please send them to [me](mailto:aaron@niskin.org)! I love riddles.
