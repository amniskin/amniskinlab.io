---
title: "First Post, An Explication"
type: post
categories:
- Meta
excerpt: "A solemn oath to the reader... An oath I will surely forget and fail to uphold. Take it as you will"
tags:
- first post
- meta
feature_image: "/assets/pics/WhaleHello.jpg"
---

This is my first blog, so I guess I should start off by explaining my motives behind writing what will probably be a sporadically updated blog. Basically, as I learn things that I find pretty difficult to find online, I'll try to explain them as best I can here. Also, since I enjoy learning math, I'm going to try to keep up a (semi) regular stream of math posts. If you have any questions, feel free to contact me. My up-to-date contact info can be found on my website [aaron.niskin.org](http://aaron.niskin.org)
