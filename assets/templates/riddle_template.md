---
title: 
type: post
excerpt: |
  A group of prisoners encounter an evil warden and his sadistic lightbulb. Who will win?
category: Riddles
tags:
- puzzles
- riddles
- fun&games
- pokeaneyeout
feature_image: /assets/pics/2018/06/02_light_bulb.jpg
---

## The scenario:


What you know:

1. 

What's your strategy?

### Hints (click to unblur)

1. There's a non-zero probability (assuming the choice of prisoner is random) that it's 100,000 years before all prisoners go into that room. So don't try to find a perfect solution. Just any solution will work.
    {: .hint}


### Some Solutions

#### Solution 1

<div class="hint" markdown="1">
</div>
